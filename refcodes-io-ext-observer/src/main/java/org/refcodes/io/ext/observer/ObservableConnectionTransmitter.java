// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.io.ext.observer;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.refcodes.component.Component;
import org.refcodes.component.ConnectionStatus;
import org.refcodes.component.ext.observer.ClosedEvent;
import org.refcodes.component.ext.observer.ConnectionObserver;
import org.refcodes.component.ext.observer.ConnectionStatusEvent;
import org.refcodes.component.ext.observer.OpenedEvent;
import org.refcodes.controlflow.ExecutionStrategy;
import org.refcodes.exception.VetoException;
import org.refcodes.io.ConnectionDatagramsTransmitter;
import org.refcodes.observer.AbstractObservable;
import org.refcodes.observer.ActionEvent;
import org.refcodes.observer.EventMetaData;
import org.refcodes.observer.Observable;

/**
 * The {@link ObservableConnectionTransmitter} extends the
 * {@link ConnectionDatagramsTransmitter} with {@link ConnectionObserver}
 * functionality.
 *
 * @param <DATA> The type of the datagram to be operated with.
 * @param <CON> The type of the connection to be used.
 */
public class ObservableConnectionTransmitter<DATA extends Serializable, CON> implements ConnectionDatagramsTransmitter<DATA, CON>, Observable<ConnectionObserver<CON>>, Component {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final Logger LOGGER = Logger.getLogger( ObservableConnectionReceiver.class.getName() );

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private ConnectionDatagramsTransmitter<DATA, CON> _connectionSender;
	private ConnectionObservable _observable;
	private EventMetaData _eventMetaData;
	private CON _source;
	private boolean _isDestroyed = false;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = new EventMetaData( this.getClass() );
		_source = toSource();
		_observable = new ConnectionObservable();
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aSource The source instance to be used when firing events in case
	 *        the source is to be different from this class' instance.
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, CON aSource ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = new EventMetaData( this.getClass() );
		_source = aSource;
		_observable = new ConnectionObservable();
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aEventMetaData The {@link EventMetaData} to be used when firing
	 *        events in case the {@link EventMetaData} is to be different from
	 *        the auto-generated {@link EventMetaData}.
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, EventMetaData aEventMetaData ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = aEventMetaData;
		_source = toSource();
		_observable = new ConnectionObservable();
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aEventMetaData The {@link EventMetaData} to be used when firing
	 *        events in case the {@link EventMetaData} is to be different from
	 *        the auto-generated {@link EventMetaData}.
	 * @param aSource The source instance to be used when firing events in case
	 *        the source is to be different from this class' instance.
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, EventMetaData aEventMetaData, CON aSource ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = aEventMetaData;
		_source = aSource;
		_observable = new ConnectionObservable();
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aExecutorService The executor service to be used when firing
	 *        {@link ActionEvent} instances in multiple threads (if null then a
	 *        default one is used).
	 * @param aExecutionStrategy The {@link ExecutionStrategy} to be used when
	 *        firing {@link ActionEvent} instance (if null then the default
	 *        {@link ExecutionStrategy#SEQUENTIAL} is used).
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = new EventMetaData( this.getClass() );
		_source = toSource();
		_observable = new ConnectionObservable( aExecutorService, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aSource The source instance to be used when firing events in case
	 *        the source is to be different from this class' instance.
	 * @param aExecutorService The executor service to be used when firing
	 *        {@link ActionEvent} instances in multiple threads (if null then a
	 *        default one is used).
	 * @param aExecutionStrategy The {@link ExecutionStrategy} to be used when
	 *        firing {@link ActionEvent} instance (if null then the default
	 *        {@link ExecutionStrategy#SEQUENTIAL} is used).
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, CON aSource, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = new EventMetaData( this.getClass() );
		_source = aSource;
		_observable = new ConnectionObservable( aExecutorService, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aEventMetaData The {@link EventMetaData} to be used when firing
	 *        events in case the {@link EventMetaData} is to be different from
	 *        the auto-generated {@link EventMetaData}.
	 * @param aExecutorService The executor service to be used when firing
	 *        {@link ActionEvent} instances in multiple threads (if null then a
	 *        default one is used).
	 * @param aExecutionStrategy The {@link ExecutionStrategy} to be used when
	 *        firing {@link ActionEvent} instance (if null then the default
	 *        {@link ExecutionStrategy#SEQUENTIAL} is used).
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, EventMetaData aEventMetaData, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = aEventMetaData;
		_source = toSource();
		_observable = new ConnectionObservable( aExecutorService, aExecutionStrategy );
	}

	/**
	 * Constructs a {@link ObservableConnectionTransmitter} with the given
	 * attributes.
	 * 
	 * @param aConnectionSender The {@link ConnectionDatagramsTransmitter} to
	 *        which the connection method calls are to be delegated to.
	 * @param aEventMetaData The {@link EventMetaData} to be used when firing
	 *        events in case the {@link EventMetaData} is to be different from
	 *        the auto-generated {@link EventMetaData}.
	 * @param aSource The source instance to be used when firing events in case
	 *        the source is to be different from this class' instance.
	 * @param aExecutorService The executor service to be used when firing
	 *        {@link ActionEvent} instances in multiple threads (if null then a
	 *        default one is used).
	 * @param aExecutionStrategy The {@link ExecutionStrategy} to be used when
	 *        firing {@link ActionEvent} instance (if null then the default
	 *        {@link ExecutionStrategy#SEQUENTIAL} is used).
	 */
	public ObservableConnectionTransmitter( ConnectionDatagramsTransmitter<DATA, CON> aConnectionSender, EventMetaData aEventMetaData, CON aSource, ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
		_connectionSender = aConnectionSender;
		_eventMetaData = aEventMetaData;
		_source = aSource;
		_observable = new ConnectionObservable( aExecutorService, aExecutionStrategy );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosed() {
		return _connectionSender.isClosed();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpened() {
		return _connectionSender.isOpened();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ConnectionStatus getConnectionStatus() {
		return _connectionSender.getConnectionStatus();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void flush() throws IOException {
		_connectionSender.flush();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void close() throws IOException {
		_connectionSender.close();
		try {
			_observable.fireEvent( new ClosedEvent<>( _eventMetaData, _source ) );
		}
		catch ( VetoException e ) {
			/* Cannot happen here */
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void open( CON aConnection ) throws IOException {
		_connectionSender.open( aConnection );
		try {
			_observable.fireEvent( new OpenedEvent<>( _eventMetaData, _source ) );
		}
		catch ( VetoException e ) {
			/* Cannot happen here */
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void transmit( DATA aDatagram ) throws IOException {
		_connectionSender.transmit( aDatagram );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isClosable() {
		return _connectionSender.isClosable();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isOpenable( CON aConnection ) {
		return _connectionSender.isOpenable( aConnection );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean hasObserver( ConnectionObserver<CON> aObserver ) {
		return _observable.hasObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean subscribeObserver( ConnectionObserver<CON> aObserver ) {
		return _observable.subscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean unsubscribeObserver( ConnectionObserver<CON> aObserver ) {
		return _observable.unsubscribeObserver( aObserver );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void destroy() {
		if ( !_isDestroyed ) {
			_isDestroyed = true;
			try {
				close();
			}
			catch ( IOException e ) {
				LOGGER.log( Level.WARNING, "Unable to close malfunctioning connection.", e );
			}
			_connectionSender = null;
			_observable.clear();
			_observable = null;
			_source = null;
			_eventMetaData = null;
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	@SuppressWarnings("unchecked")
	private CON toSource() {
		try {
			final Field theField = getClass().getField( "_source" );
			if ( theField.getGenericType() instanceof Class<?> theClass ) {
				return theClass.isAssignableFrom( getClass() ) ? (CON) this : null;
			}

		}
		catch ( Exception ignore ) { /* ignore */ }
		return null;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The {@link ConnectionObservable} extends the {@link AbstractObservable}
	 * to make the using class {@link ActionEvent} aware.
	 */
	private class ConnectionObservable extends AbstractObservable<ConnectionObserver<CON>, ConnectionStatusEvent<CON>> {

		// /////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////

		private final ExecutionStrategy _executionStrategy;

		// /////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new connection observable.
		 */
		public ConnectionObservable() {
			_executionStrategy = ExecutionStrategy.SEQUENTIAL;
		}

		/**
		 * Instantiates a new connection observable.
		 *
		 * @param aExecutorService the executor service
		 * @param aExecutionStrategy the execution strategy
		 */
		public ConnectionObservable( ExecutorService aExecutorService, ExecutionStrategy aExecutionStrategy ) {
			super( aExecutorService );
			_executionStrategy = ( aExecutionStrategy != null ) ? aExecutionStrategy : ExecutionStrategy.SEQUENTIAL;
		}

		// /////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int size() {
			return super.size();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public boolean isEmpty() {
			return super.isEmpty();
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public void clear() {
			super.clear();
		}

		// /////////////////////////////////////////////////////////////////////
		// HOOKS:
		// /////////////////////////////////////////////////////////////////////

		/**
		 * Same as {@link #fireEvent(ConnectionStatusEvent, ExecutionStrategy)}
		 * with a predefined {@link ExecutionStrategy}.
		 *
		 * @param aEvent the event to be fired.
		 * 
		 * @return Returns true, if dispatching the event was successful.
		 * 
		 * @throws VetoException Thrown in case there was a veto.
		 * 
		 * @see #fireEvent(ConnectionStatusEvent, ExecutionStrategy)
		 */
		protected boolean fireEvent( ConnectionStatusEvent<CON> aEvent ) throws VetoException {
			return super.fireEvent( aEvent, _executionStrategy );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		protected boolean fireEvent( ConnectionStatusEvent<CON> aEvent, ConnectionObserver<CON> aObserver, ExecutionStrategy aExecutionStrategy ) throws VetoException {
			if ( aEvent instanceof OpenedEvent ) {
				aObserver.onOpendEvent( (OpenedEvent<CON>) aEvent );
			}
			else if ( aEvent instanceof ClosedEvent ) {
				aObserver.onClosedEvent( (ClosedEvent<CON>) aEvent );
			}
			aObserver.onEvent( aEvent );
			return true;
		}
	}
}
