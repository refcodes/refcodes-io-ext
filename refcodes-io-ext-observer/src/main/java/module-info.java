module org.refcodes.io.ext.observer {
	requires transitive org.refcodes.exception;
	requires transitive org.refcodes.component;
	requires transitive org.refcodes.component.ext.observer;
	requires transitive org.refcodes.controlflow;
	requires transitive org.refcodes.io;
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.observer;
	requires java.logging;

	exports org.refcodes.io.ext.observer;
}
